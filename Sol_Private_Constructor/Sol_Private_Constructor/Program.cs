﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Private_Constructor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test testObj = new Test();//not accessed bcoz of private constructor

            Test testObj1 = Test.CreateInstance();
            testObj1.Display();
        }
    }

    public class Test
    {
        private static Test testObj = null;

        private Test()
        {

        }

        public void Display()
        {
            Console.WriteLine("hello world");
        }

        public static Test CreateInstance()
        {
            return testObj = new Test();
        }
    }

    
}
